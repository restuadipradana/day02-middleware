<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth' , 'check_role'])->group(function(){
    Route::get('/route-1', 'TestController@page1')->name('route1');
    Route::get('/route-2', 'TestController@page2')->name('route2');
    Route::get('/route-3', 'TestController@page3')->name('route3');
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
