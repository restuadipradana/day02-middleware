<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class TestController extends Controller
{
    public function page1()
    {
        //dd(Auth::user());
        return 'berhasil masuk ke route 1';
    }

    public function page2()
    {
        return 'berhasil masuk ke route 2';
    }

    public function page3()
    {
        return 'berhasil masuk ke route 3';
    }
}
