<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = \Route::current()->getName();  //"route1"
        $user = \Auth::user();
        //dd($user);

        if($user->cek_route($routeName)){
            return $next($request);

        }
        return abort(403);

    }
}
