<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_roles', 'role_id', 'user_id');
    }

    public function can_accesses(){
        return $this->belongsToMany('App\CanAccess', 'role_can_accesses', 'role_id', 'can_access_id');
    }
}
