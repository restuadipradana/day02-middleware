<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CanAccess extends Model
{
    public function roles(){
        return $this->belongsToMany('App\Role', 'role_can_accesses', 'can_access_id', 'role_id');
    }
}
