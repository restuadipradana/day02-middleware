<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\CanAccess;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function cek_route($routeName)
    {
        $cek = false;
        $can_access_id = CanAccess::where('route_name', $routeName)->first()->id;   //get can access id (routenameid) by route name
        $roles = $this->roles;                                                      //get user role yang login (1 user bisa 1 atau banyak role) [admin] atau bisa [admin, sa]
        if($roles){
            foreach($roles as $role){
                //dd($role);
                //dd($role->can_accesses);
                foreach($role->can_accesses as $can_access){                        //get route yg dapat diakses oleh 1 role (role 1 ada 3 route yg bisa di akses)
                    //dd($role->can_accesses);
                    if($can_access_id == $can_access->id){
                        $cek = true;
                    }
                    
                }
            }
        }
        
        return $cek;
    }
}
